import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { data } from 'src/shared/data/data';
import { RegisterModel } from 'src/shared/models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly url = "http://localhost:9001/market/users";
  private readonly registers: Array<RegisterModel> = data;
  constructor(private readonly http: HttpClient) { }

  getRegisters(): Array<RegisterModel> {
    return [...this.registers];
  }

  pushRegister(user: RegisterModel) {
    this.registers.push(user)
  }

  getOneRegister(userName: string): RegisterModel | undefined {
    return this.registers.find((user) => userName === user.user);
  }

  saveUser(register: RegisterModel): Observable<RegisterModel> {
    return this.http.post<RegisterModel>(this.url, register);
  }
}

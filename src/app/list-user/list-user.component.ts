import { Component, Input } from '@angular/core';
import { RegisterModel } from 'src/shared/models/user.interface';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent {

  @Input() set setNewRegister(register: RegisterModel | undefined) {
    if(register) this.pushNewRegister(register);
  };
  // @Input() newRegister: RegisterModel;

  registers: RegisterModel[];

  constructor(private readonly registerService: UserService) {
    this.registers = registerService.getRegisters();
  }

  pushNewRegister(register: RegisterModel) {
    this.registerService.pushRegister(register);
    this.registers = this.registerService.getRegisters();
  }
}

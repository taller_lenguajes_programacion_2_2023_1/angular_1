import { Component } from '@angular/core';
import { RegisterModel } from 'src/shared/models/user.interface';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-project';
  emailLabel = 'Correo';
  passLabel = 'Contraseña';

  emailName = 'emailName';
  classInputEmail = 'form-control';

  userName = 'jesus.gomez';
  passValue = '1234';

  register: RegisterModel | undefined;

  constructor(private readonly registerService: UserService) {}

  onClickSaveUser(): void {
    // logic -- valid form // transformers  // service called
    this.registerService.saveUser({user: this.userName, pass: this.passValue, status: false})
      .subscribe({
        next: (backResponse) => {
          //logic
          alert('user saved with success');
          console.log('backResponse success', backResponse)
        },
        error: (error) => console.log('backResponse Failed', error)
      });
  }
}

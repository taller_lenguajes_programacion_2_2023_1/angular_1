export interface RegisterModel {
  user: string;
  pass: string;
  status?: boolean;
  rol?: string;
}

export interface ExampleModel {
  property: string;
  optionalPropety?: string;
  array: Array<string>;
  object: {
    name: string;
    lastName: string;
  }
}
